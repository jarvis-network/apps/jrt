{ pkgs ? import <nixpkgs> { } }:
with pkgs;
mkShell {
  buildInputs = [
    fish
    git
    nodejs-12_x
    yarn
    gnumake
    python3
    gccStdenv
    eudev
    libusb1.dev
    pkg-config
    zip
  ];

  shellHook = ''
    echo 'Welcome to the JRT dev environment'
  '';
}
