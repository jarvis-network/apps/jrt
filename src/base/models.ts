import _ from 'lodash';
import BN from 'bn.js';
import { SortedArray } from './sorting';
import {
  assertIsAddress,
  assertIsArray,
  assertIsString,
  fromBNToDecimalString,
  sumBN,
  toBN,
  mapSumBN,
  writeJsonToFile,
  readJsonFromFile,
} from './utils';
import { pick } from './meta';

export interface Participant {
  address: string;
}

export interface Holder extends Participant {
  balance: BN;
}

export interface Recipient extends Participant {
  amount: BN;
  email?: string;
  reason?: string;
}

export interface RecipientJsonEntry extends Participant {
  amount: string;
}

export interface RecipientJsonEntryWithAmountInWei extends Participant {
  amountInWei: string;
}

export type AnyRecipientType = Partial<
  RecipientJsonEntry & RecipientJsonEntryWithAmountInWei
>;

export function recipientToJson(
  r: Recipient,
  amountInWei: boolean = true,
): RecipientJsonEntry | RecipientJsonEntryWithAmountInWei {
  const { address, amount, ...rest } = r;
  return {
    ...rest, // pass-through all remaining props
    address: address.toLowerCase(),
    ...(amountInWei
      ? { amountInWei: amount.toString(10) }
      : { amount: fromBNToDecimalString(amount) }),
  };
}

function normalizeNumberString(x: unknown): string {
  return assertIsString(x).trim().replace(',', '');
}

export function recipientFromJson(json: unknown) {
  const { selected, rest } = pick<AnyRecipientType>()(
    json,
    'amount',
    'address',
    'amountInWei',
  );

  let amount;

  if ('amount' in selected) {
    amount = BN.isBN(selected.amount)
      ? selected.amount
      : toBN(normalizeNumberString(selected.amount));
  } else if ('amountInWei' in selected) {
    amount = new BN(normalizeNumberString(selected.amountInWei));
  } else {
    throw Error(
      `Expected field named 'amount' or 'amountInWei', but found: ${json}`,
    );
  }

  return {
    ...rest,
    address: assertIsAddress(assertIsString(selected.address).trim()),
    amount,
  };
}

export function recipientListFromJson(list: unknown): Recipient[] {
  return assertIsArray(list).map(recipientFromJson);
}

export function recipientListToJson(
  list: Recipient[],
  amountInWei = true,
): (RecipientJsonEntry | RecipientJsonEntryWithAmountInWei)[] {
  return list.map(x => recipientToJson(x, amountInWei));
}

export function orderRecipientByAddress(a: Participant, b: Participant) {
  return a.address.toLowerCase().localeCompare(b.address.toLowerCase());
}

declare global {
  interface Array<T> {
    sortByAddress<U extends Participant>(this: U[]): SortedArray<U, string>;
  }

  interface Array<T> {
    sortByAmount<U extends Recipient>(this: T[]): SortedArray<U, string>;
  }
}

Array.prototype.sortByAddress = function <U extends Participant>() {
  return SortedArray.createFromUnsorted<U, string>(
    this,
    orderRecipientByAddress,
    x => x.address,
  );
};

Array.prototype.sortByAmount = function <U extends Recipient>() {
  return SortedArray.createFromUnsorted<U, string>(
    this,
    (a, b) => a.amount.cmp(b.amount),
    x => x.amount.toString('hex'),
  );
};

export function groupByAddressAndSortByAmount(list: Recipient[]) {
  return _.chain(list)
    .groupBy(r => r.address.toLowerCase())
    .map<Recipient>((instances, _) => ({
      ...instances[0],
      amount: sumBN(instances.map(inst => inst.amount)),
    }))
    .value()
    .sortByAmount();
}

export function adjustAmounts(
  array: Recipient[],
  amountMapFn: (amount: BN) => BN,
): Recipient[] {
  return array.map(({ amount, ...rest }) => ({
    ...rest,
    amount: amountMapFn(amount),
  }));
}

export function sumAmounts(array: Recipient[]): BN {
  return mapSumBN(array, x => x.amount);
}

export async function saveRecipientsFile(
  list: Recipient[],
  filePath: string,
  amountInWei = true,
) {
  const result = recipientListToJson(list, amountInWei);
  return await writeJsonToFile(filePath, result);
}

export async function readRecipientsFile(path: string) {
  const json = await readJsonFromFile(path);
  return recipientListFromJson(json);
}
