import { getInfuraWeb3 } from '../apis/infura';
import { env } from '../base/config';
import { getContract } from '../base/web3';
import _ from 'lodash';
import { promises as fs } from 'fs';
import { resolve } from 'path';
import Web3 from 'web3';

async function getEvents(web3: Web3, contractAddress: string, name: string) {
  const contract = await getContract(contractAddress, web3);
  const allEvents = await contract.getPastEvents('allEvents', {
    fromBlock: 0,
    toBlock: 'latest',
  });
  await fs.writeFile(
    resolve(`./${name}.json`),
    JSON.stringify(allEvents, null, 2),
  );
}

export async function main() {
  const web3 = getInfuraWeb3('mainnet');
  console.log('Fetching JRT Uni-V1 events');
  await getEvents(
    web3,
    env.ethAddresses.liquidityPools.uniswap_v1_JRT_ETH,
    'jrt-uniswap-all-events',
  );
  console.log('Fetching JRT Bancor smart relayer events');
  await getEvents(
    web3,
    env.ethAddresses.liquidityPools.bancor_usdb,
    'jrt-bancor-smart-relayer-all-events',
  );
  console.log('Fetching JRT Bancor pool events');
  await getEvents(web3, env.ethAddresses.jrtBancorPool, 'jrt-bancor-pool');
}

(async () => {
  await main();
  console.log('All events fetched.');
})();
