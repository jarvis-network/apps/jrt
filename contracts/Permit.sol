pragma solidity ^0.5.0;

import './ERC20.sol';

contract Permit is ERC20 {
  bytes32 public DOMAIN_SEPARATOR;

  bytes32 public PERMIT_TYPEHASH;

  mapping(address => uint256) public nonces;

  bool internal upgradedToV2;

  function initializePermit(string memory name, uint256 chainId) internal {
    DOMAIN_SEPARATOR = keccak256(
      abi.encode(
        keccak256(
          'EIP712Domain(string name,string version,uint256 chainId,address verifyingContract)'
        ),
        keccak256(bytes(name)),
        keccak256(bytes('1')),
        chainId,
        address(this)
      )
    );
    PERMIT_TYPEHASH = keccak256(
      'Permit(address holder,address spender,uint256 nonce,uint256 expiry,bool allowed)'
    );
  }

  function permit(
    address holder,
    address spender,
    uint256 nonce,
    uint256 expiry,
    bool allowed,
    uint8 v,
    bytes32 r,
    bytes32 s
  ) external {
    bytes32 digest =
      keccak256(
        abi.encodePacked(
          '\x19\x01',
          DOMAIN_SEPARATOR,
          keccak256(
            abi.encode(PERMIT_TYPEHASH, holder, spender, nonce, expiry, allowed)
          )
        )
      );
    require(holder != address(0), 'Jrt/invalid-address-0');
    require(holder == ecrecover(digest, v, r, s), 'Jrt/invalid-permit');
    require(expiry == 0 || now <= expiry, 'Jrt/permit-expired');
    require(nonce == nonces[holder]++, 'jrt/invalid-nonce');
    uint256 wad = allowed ? uint256(-1) : 0;
    _allowed[holder][spender] = wad;
    emit Approval(holder, spender, wad);
  }

  uint256[50] private ______gap;
}
