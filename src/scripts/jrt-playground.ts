import { getInfuraWeb3 } from '../apis/infura';
import { env } from '../base/config';
import { getContract } from '../base/web3';
import { JarvisRewardToken } from '../contracts/JarvisRewardToken';
import { weiToNumber } from '../base/utils';

export async function main() {
  const web3 = getInfuraWeb3('mainnet');
  const contract = await getContract<JarvisRewardToken>(
    env.ethAddresses.jrtContract,
    web3,
    { type: 'build-artifact', contractName: 'JarvisRewardToken' },
  );
  const amount = await contract.methods.totalSupply().call();
  console.log(`Total supply: ${weiToNumber(amount).toLocaleString()} JRT`);
}

(async () => {
  await main();
  console.log('All events fetched.');
})();
