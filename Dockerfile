# The BASE_IMAGE build argument allows substituting the default base image -
# at the moment `node:lts-alpine` - with another one, more suitable for
# development.
#
# For example, by the following script (from
# github.com/PetarKirov/dotfiles/utils) will create a more well-featured dev
# environment (that includes niceties such as Fish shell, NeoVim and so on):
#     docker-dev-env.sh node:lts-buster
#
# Then the image can be built like so:
#     docker build --build-arg BASE_IMAGE=zlx/node:lts-alpine_dev -t jrt:dev .
#
# As having more dev utilities installed in the image is purely optional, by
# default they are not included and so the image is smaller and the command to
# build is slightly simpler:
#     docker build -t jrt .


ARG BASE_IMAGE=node:lts-alpine
FROM ${BASE_IMAGE}
RUN apk add g++ git make python3 linux-headers eudev-dev libusb-dev
WORKDIR /src
