import program from 'commander';
import { resolve } from 'path';
import { assertIsString } from '../base/utils';
import { saveRewardsFileBaseOnSnapshots, calculateAllRewards } from './actions/liquidity-reward-calculation';
import BN from 'bn.js';
import { toWei } from 'web3-utils';
import { parseDate } from '../base/formatting';

program
  .requiredOption('--date <date>', 'Date relative to which to calculate tokens')

program.parse(process.argv);

(async () => {
  console.log('Starting program.');
  calculateAllRewards(parseDate(assertIsString(program.date)));
  console.log('Program execution complete.');
})();
