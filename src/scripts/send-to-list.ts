require('./runtime_config');
import program from 'commander';
import { distributeJrtTokens } from './actions/distribute';
import {
  assertIsString,
  assertIsBoolean,
  assertIsUndefinedOrFiniteFloat,
  assertIsUndefinedOrInteger,
} from '../base/utils';
import { readRecipientsFile } from '../base/models';
import { env } from '../base/config';

const getInt = (input: string | undefined, defaultValue: number) =>
  assertIsUndefinedOrInteger(input ?? defaultValue);

program
  .requiredOption(
    '-i, --input <path>',
    'Path to a JSON file containing array of recipients',
  )
  .option('--dry-run', 'Simulate sending, without actually doing it', false)
  .option('--no-verify', "Don't verify recipient balances after sending", false)
  .option('--gas-price <number>', 'Override automatic gas price determination')
  .option('--gas-limit <number>', 'Override automatic gas estimation')
  .option(
    '--start-index <number>',
    'Specify the starting index for the batch tx',
  )
  .option('--end-index <number>', 'Specify the ending index for the batch tx')
  .option('--rpc-url <URL>', 'Custom RPC endpoint. If not specified Infura endpoint is used.')
  .option(
    '--batch-size <number>',
    'Max number of recipients to attempt to send to in one batch',
    getInt,
    env.constants.BATCH_TRANSACTION_COUNT,
  )
  .parse(process.argv);

async function main() {
  const {
    input,
    dryRun,
    verify,
    startIndex,
    endIndex,
    batchSize,
    gasPrice,
    gasLimit,
    rpcUrl,
  } = program;
  const recipients = await readRecipientsFile(assertIsString(input));
  await distributeJrtTokens({
    customRpcUrl: rpcUrl,
    dryRun: assertIsBoolean(dryRun),
    verify: assertIsBoolean(verify),
    gasPrice: assertIsUndefinedOrFiniteFloat(gasPrice),
    gasLimit: assertIsUndefinedOrFiniteFloat(gasLimit),
    listParams: {
      recipients,
      startIndex: assertIsUndefinedOrInteger(startIndex),
      endIndex: assertIsUndefinedOrInteger(endIndex),
      batchSize: assertIsUndefinedOrInteger(batchSize),
    },
  });
}

(async () => await main())();
