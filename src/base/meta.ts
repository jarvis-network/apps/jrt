export const t = <T extends any[]>(...args: T): T => args;

export type Keys<T> = keyof T;
export type Values<T> = T[Keys<T>];

export function hasProp<O extends object, K extends PropertyKey>(
  obj: O,
  propKey: K,
): obj is O & { [key in K]: Values<O> } {
  return propKey in obj;
}

export function getProp<O extends {}, K extends PropertyKey>(
  obj: O,
  propKey: K,
): (O & { [key in K]: unknown })[K] {
  if (typeof obj === 'object' && propKey in obj) {
    return (obj as any)[propKey];
  }
  throw new Error(
    `Expected property '${propKey}' in object '${JSON.stringify(obj)}'`,
  );
}

export function removeValue<T>(array: T[], value: T): boolean {
  const index = array.indexOf(value);
  if (index === -1) {
    return false;
  }
  array.splice(index, 1);
  return true;
}

export type UnknownValues<T> = {
  [P in keyof T]: unknown;
};

export function pick<T>() {
  return <K extends keyof T>(
    obj: unknown,
    ...keys: K[]
  ): { selected: UnknownValues<Pick<T, K>>; rest: {} } => {
    const selected: any = {},
      rest: any = {};
    for (const [key, value] of Object.entries(obj)) {
      if (removeValue(keys as string[], key)) {
        selected[key] = value;
      } else {
        rest[key] = value;
      }
    }
    return { selected, rest };
  };
}
