import { makeNetworkDef } from './deployment';

export const networks = {
  ropsten: makeNetworkDef({ networkId: 3, ledger: false }),
  kovan: makeNetworkDef({ networkId: 42, ledger: false }),
  ropsten_ledger: makeNetworkDef({ networkId: 3, ledger: true }),
  kovan_ledger: makeNetworkDef({ networkId: 42, ledger: true }),
  mainnet_ledger: makeNetworkDef({ networkId: 1, ledger: true }),
  local: {
    host: '127.0.0.1',
    port: 8545,
    network_id: '*', // Match any network id
  },
};

export const compilers = {
  solc: {
    version: '0.5.7', // Fetch exact version from solc-bin (default: truffle's version)
    // docker: true,        // Use "0.5.1" you've installed locally with docker (default: false)
    evmVersion: 'petersburg',
    settings: {
      // See the solidity docs for advice about optimization and evmVersion
      optimizer: {
        enabled: true,
        runs: 200,
      },
    },
  },
};
