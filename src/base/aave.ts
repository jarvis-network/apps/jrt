import BN from 'bn.js';
import Web3 from 'web3';
import { SortedArray } from './sorting';
import type { AToken } from '../contracts/AToken';
import {
  getBlockTimestamp,
} from './web3';

interface Filter {
  [key: string]: number | string | string[] | number[];
}

export async function getAllRedirectEvents(
  contract: AToken,
  address?: string,
  filter?: Filter,
  fromBlock = 0,
  toBlock: number | 'latest' = 'latest',
) {
  const events = await contract.getPastEvents('InterestStreamRedirected', {
    address,
    filter,
    fromBlock,
    toBlock,
  });
  type EventType = Parameters<
    Parameters<typeof contract.events.InterestStreamRedirected>[1]
  >[1];
  return (events as unknown) as EventType[];
}

export async function getAllWithdrawEvents(
  contract: AToken,
  address?: string,
  filter?: Filter,
  fromBlock = 0,
  toBlock: number | 'latest' = 'latest',
) {
  const events = await contract.getPastEvents('Redeem', {
    address,
    filter,
    fromBlock,
    toBlock,
  });
  type EventType = Parameters<Parameters<typeof contract.events.Redeem>[1]>[1];
  return (events as unknown) as EventType[];
}

export async function getAllDepositEvents(
  contract: AToken,
  address?: string,
  filter?: Filter,
  fromBlock = 0,
  toBlock: number | 'latest' = 'latest',
) {
  const events = await contract.getPastEvents('MintOnDeposit', {
    address,
    filter,
    fromBlock,
    toBlock,
  });
  type EventType = Parameters<Parameters<typeof contract.events.MintOnDeposit>[1]>[1];
  return (events as unknown) as EventType[];
}

export async function getAllTransferEvents(
  contract: AToken,
  address?: string,
  filter?: Filter,
  fromBlock = 0,
  toBlock: number | 'latest' = 'latest',
) {
  const events = await contract.getPastEvents('BalanceTransfer', {
    address,
    filter,
    fromBlock,
    toBlock,
  });
  type EventType = Parameters<Parameters<typeof contract.events.BalanceTransfer>[1]>[1];
  return (events as unknown) as EventType[];
}

export type TimestampedRedirectEvent = {
  blockNumber: number;
  blockTimestamp: number;
  from: string;
  to: string;
  amount: string;
};

export type TimestampDepositOrWithdrawEvent = {
  blockNumber: number;
  blockTimestamp: number;
  from: string;
  amount: string;
};

export async function getAllRedirectInfo(
  web3: Web3,
  contract: AToken,
  address?: string,
  filter?: Filter,
  fromBlock = 0,
  toBlock: number | 'latest' = 'latest',
  decimals:number = 18,
) {
  console.log('Downloading Redirection TX events');
  const events = await getAllRedirectEvents(
    contract,
    address,
    filter,
    fromBlock,
    toBlock,
  );
  console.log(`Got ${events.length} events, fetching respective txs...`);
  const eventsWithTimestamp = await Promise.all(
    events.map(async e => {
      const tx = await web3.eth.getTransaction(e.transactionHash);
      const blockNumber = tx.blockNumber;
      let blockTimestamp = await getBlockTimestamp(web3, blockNumber);
      return {
        from: e.returnValues._from.toLowerCase(),
        to: e.returnValues._to.toLowerCase(),
        amount: (new BN (e.returnValues._redirectedBalance).mul(new BN (10).pow(new BN (18).sub(new BN(decimals))))).toString(),
        blockNumber,
        blockTimestamp,
      };
    }),
  );
  const sorted = SortedArray.createFromSortedUnsafe(
    eventsWithTimestamp,
    (a, b) => a.blockTimestamp - b.blockTimestamp,
    tx => tx.blockTimestamp,
  );
  return sorted;
}

export async function getAllWithdrawInfo(
  web3: Web3,
  contract: AToken,
  address?: string,
  filter?: Filter,
  fromBlock = 0,
  toBlock: number | 'latest' = 'latest',
  decimals: number = 18
) {
  console.log('Downloading Withdrawv TX events');
  const events = await getAllWithdrawEvents(
    contract,
    address,
    filter,
    fromBlock,
    toBlock,
  );
  console.log(`Got ${events.length} events, fetching respective txs...`);
  const eventsWithTimestamp= await Promise.all(
    events.map(async e => {
      const tx = await web3.eth.getTransaction(e.transactionHash);
      const blockNumber = tx.blockNumber;
      let blockTimestamp = await getBlockTimestamp(web3, blockNumber);
      return {
        from: e.returnValues._from.toLowerCase(),
        amount: (new BN (e.returnValues._value).mul(new BN (10).pow(new BN (18).sub(new BN(decimals))))).toString(),
        blockNumber,
        blockTimestamp,
      };
    }),
  );
  const sorted = SortedArray.createFromSortedUnsafe(
    eventsWithTimestamp,
    (a, b) => a.blockTimestamp - b.blockTimestamp,
    tx => tx.blockTimestamp,
  );
  return sorted;
}

export async function getAllDepositInfo(
  web3: Web3,
  contract: AToken,
  address?: string,
  filter?: Filter,
  fromBlock = 0,
  toBlock: number | 'latest' = 'latest',
  decimals: number = 18
) {
  console.log('Downloading Deposit TX events');
  const events = await getAllDepositEvents(
    contract,
    address,
    filter,
    fromBlock,
    toBlock,
  );
  console.log(`Got ${events.length} events, fetching respective txs...`);
  const eventsWithTimestamp = await Promise.all(
    events.map(async e => {
      const tx = await web3.eth.getTransaction(e.transactionHash);
      const blockNumber = tx.blockNumber;
      let blockTimestamp = await getBlockTimestamp(web3, blockNumber);
      return {
        from: e.returnValues._from.toLowerCase(),
        amount: (new BN (e.returnValues._value).mul(new BN (10).pow(new BN (18).sub(new BN(decimals))))).toString(),
        blockNumber,
        blockTimestamp,
      };
    }),
  );
  const sorted = SortedArray.createFromSortedUnsafe(
    eventsWithTimestamp,
    (a, b) => a.blockTimestamp - b.blockTimestamp,
    tx => tx.blockTimestamp,
  );
  return sorted;
}


export async function getAllTransferInfo(
  web3: Web3,
  contract: AToken,
  address?: string,
  filter?: Filter,
  fromBlock = 0,
  toBlock: number | 'latest' = 'latest',
  decimals: number = 18
) {
  console.log('Downloading Transfer TX events');
  const events = await getAllTransferEvents(
    contract,
    address,
    filter,
    fromBlock,
    toBlock,
  );
  console.log(`Got ${events.length} events, fetching respective txs...`);
  const eventsWithTimestamp = await Promise.all(
    events.map(async e => {
      const tx = await web3.eth.getTransaction(e.transactionHash);
      const blockNumber = tx.blockNumber;
      let blockTimestamp = await getBlockTimestamp(web3, blockNumber);
      return {
        from: e.returnValues._from.toLowerCase(),
        to: e.returnValues._to.toLowerCase(),
        amount: (new BN (e.returnValues._value).mul(new BN (10).pow(new BN (18).sub(new BN(decimals))))).toString(),
        blockNumber,
        blockTimestamp,
      };
    }),
  );
  const sorted = SortedArray.createFromSortedUnsafe(
    eventsWithTimestamp,
    (a, b) => a.blockTimestamp - b.blockTimestamp,
    tx => tx.blockTimestamp,
  );
  return sorted;
}

