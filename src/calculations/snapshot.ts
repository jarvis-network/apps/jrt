import BN from 'bn.js';
import { pick } from '../base/meta';
import {
  assertIsNumber,
  assertIsAddress,
  assertIsString,
  assertIsObject,
  writeJsonToFile,
  readJsonFromFile,
} from '../base/utils';

export type SnapshotInfo = {
  startBlock: number; // the first block included in the interval (inclusive) | 05:00:10
  endBlock: number; // the last block included in the interval (inclusive)  | 05:59:57
  startingTime: number;
  endingTime: number;
};

export type ParticipantBalance = {
  balance: BN;
};

export type ParticipantWeight = {
  weight: BN;
};

export type Participant = ParticipantBalance & ParticipantWeight;

export type ParticipantWithState = Participant & {
  timestampOfLastTransfer?: number;
  updated?: boolean;
};

export type Participants = {
  [address: string]: Participant;
};

export type ParticipantWeights = {
  [address: string]: ParticipantWeight;
};

export type ParticipantsWithState = {
  [address: string]: ParticipantWithState;
};

export type SnapshotWeightsOnly = SnapshotInfo & {
  participants: ParticipantWeights;
};

export type Snapshot = SnapshotInfo & {
  participants: Participants;
};

export type SnapshotWithName = Snapshot & {
  name: string;
};

export type SnapshotWithState = SnapshotInfo & {
  participants: ParticipantsWithState;
};

export async function writeSnapshotFile(path: string, snapshot: Snapshot) {
  return await writeJsonToFile(path, snapshot);
}

export async function readSnapshotFile(path: string) {
  const json = await readJsonFromFile(path);
  return parseSnapshotFromJson(json);
}

export function parseSnapshotFromJson(json: unknown): Snapshot {
  const {
    selected: { startBlock, endBlock, startingTime, endingTime, participants },
  } = pick<Snapshot>()(
    json,
    'startBlock',
    'endBlock',
    'startingTime',
    'endingTime',
    'participants',
  );

  return {
    startBlock: assertIsNumber(startBlock),
    endBlock: assertIsNumber(endBlock),
    startingTime: assertIsNumber(startingTime),
    endingTime: assertIsNumber(endingTime),
    participants: Object.keys(assertIsObject(participants)).reduce(
      (set, address) => {
        const {
          selected: { balance, weight },
        } = pick<Snapshot['participants'][string]>()(
          assertIsObject(participants)[address],
          'balance',
          'weight',
        );
        set[assertIsAddress(address)] = {
          balance: new BN(assertIsString(balance), 'hex'),
          weight: new BN(assertIsString(weight), 'hex'),
        };
        return set;
      },
      {} as Snapshot['participants'],
    ),
  };
}
