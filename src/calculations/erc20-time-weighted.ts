import BN from 'bn.js';
import Web3 from 'web3';
import { assert } from 'console';
import { zero } from '../base/utils';
import { getBlockTimestamp, getClosestBlock } from '../base/web3';
import { getAllTransferInfo } from '../base/erc20';
import type { ERC20 } from '../contracts/ERC20';
import type { Snapshot, SnapshotWithState } from './snapshot';

export async function calculate(
  web3: Web3,
  erc20: ERC20,
  previousSnapshot: Snapshot,
  startTimestamp: number,
  duration: number,
): Promise<Snapshot> {
  const firstNewBlockTimestamp = await getBlockTimestamp(
    web3,
    previousSnapshot.endBlock + 1,
  );
  assert(
    startTimestamp >= previousSnapshot.endingTime &&
      startTimestamp <= firstNewBlockTimestamp,
    'Wrong starting time',
  );
  const endTimestamp = startTimestamp + duration;
  const toBlock = await getClosestBlock(
    web3,
    previousSnapshot.endBlock,
    endTimestamp,
  );
  // all transfer events that happened during this time interval from everyone
  const allTxEvents = await getAllTransferInfo(
    web3,
    erc20,
    undefined,
    previousSnapshot.endBlock + 1, // start block
    toBlock, // end block
  );

  const existingUsers = Object.keys(
    previousSnapshot.participants,
  ).filter(user =>
    new BN(previousSnapshot.participants[user].balance, 'hex').gt(zero),
  );

  const newSnapshot: SnapshotWithState = {
    startBlock: previousSnapshot.endBlock + 1,
    endBlock: toBlock,
    startingTime: startTimestamp,
    endingTime: endTimestamp,
    participants: Object.fromEntries(
      existingUsers.map(address => [
        address,
        previousSnapshot.participants[address],
      ]),
    ),
  };

  for (let participantAddress in newSnapshot.participants) {
    newSnapshot.participants[participantAddress] = {
      balance: new BN(
        newSnapshot.participants[participantAddress].balance,
        'hex',
      ),
      weight: zero,
      timestampOfLastTransfer: startTimestamp,
      updated: false,
    };
  }

  for (let event of allTxEvents.array) {
    const fromAddress = event.from.toLowerCase();
    const toAddress = event.to.toLowerCase();
    const senderBalance =
      newSnapshot.participants[fromAddress]?.balance ?? zero;
    const senderWeight = newSnapshot.participants[fromAddress]?.weight ?? zero;
    const senderLastTx =
      newSnapshot.participants[fromAddress]?.timestampOfLastTransfer ??
      startTimestamp;
    const recipientBalance =
      newSnapshot.participants[toAddress]?.balance ?? zero;
    const recipientWeight = newSnapshot.participants[toAddress]?.weight ?? zero;
    const recipientLastTx =
      newSnapshot.participants[toAddress]?.timestampOfLastTransfer ??
      startTimestamp;
    const value = new BN(event.value);
    const timestamp = event.blockTimestamp;

    newSnapshot.participants[fromAddress] = {
      balance: senderBalance.sub(value),
      weight: senderWeight.add(
        senderBalance.mul(new BN(timestamp - senderLastTx)),
      ), //Fix: weight is a growing function and must be calculated on the previous interval of time
      timestampOfLastTransfer: timestamp,
      updated: true,
    };

    newSnapshot.participants[toAddress] = {
      balance: recipientBalance.add(value),
      weight: recipientWeight.add(
        recipientBalance.mul(new BN(timestamp - recipientLastTx)),
      ), //Fix: weight must be calculated on the previous interval of time
      timestampOfLastTransfer: timestamp,
      updated: true,
    };
  }

  delete newSnapshot.participants['0x0000000000000000000000000000000000000000'];
  delete newSnapshot.participants[erc20.options.address.toLowerCase()];

  // calculate weight for existing participants which didn't make any transfers
  for (let participant of Object.values(newSnapshot.participants)) {
    if (participant.updated === true) {
      participant.weight = participant.weight.add(
        participant.balance.mul(
          new BN(endTimestamp - participant.timestampOfLastTransfer),
        ),
      );
    } else {
      participant.weight = participant.balance.mul(
        new BN(endTimestamp - startTimestamp),
      );
    }
  }

  return newSnapshot;
}
