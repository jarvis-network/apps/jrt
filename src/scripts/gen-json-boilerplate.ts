import fs from 'fs';
import { assertIsString } from '../base/utils';

(() => {
  const path = assertIsString(process.argv[2]);
  const length = Number.parseInt(process.argv[3] ?? '1');
  const e = '  { "address": "", "email": "", "amount": "" }';
  const entries = Array.apply(null, { length })
    .map((_: null) => e)
    .join(',\n');
  fs.writeFileSync(path, `[\n${entries}\n]\n`);
  console.log(`Saved file with ${length} empty entries to '${path}'.`);
})();
