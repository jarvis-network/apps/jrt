import program from 'commander';

import { env } from '../base/config';
import { assertIsString, assertIsAddress } from '../base/utils';
import { makeErc20Snapshot } from './actions/make-erc20-snapshot';
import { hasProp } from '../base/meta';

program
  .requiredOption(
    '-c, --contract <all-pools|POOL-NAME|ADDRESS>',
    'Name or address of ERC20 conntract to snapshot',
  )
  .option('-o, --output <path>', 'Output path');

program.parse(process.argv);
const contract = assertIsString(program.contract);

(async () => {
  console.log('Starting program.');
  if (contract === 'all-pools') {
    await Promise.all(
      Object.entries(env.ethAddresses.liquidityPools).map(([pool, address]) =>
        makeErc20Snapshot(pool, address),
      ),
    );
  } else if (hasProp(env.ethAddresses.liquidityPools, contract)) {
    await makeErc20Snapshot(
      contract,
      env.ethAddresses.liquidityPools[contract],
    );
  } else {
    await makeErc20Snapshot(contract, assertIsAddress(contract));
  }
  console.log('Program execution complete.');
})();
