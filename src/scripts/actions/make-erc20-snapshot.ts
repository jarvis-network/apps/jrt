import { fromWei } from 'web3-utils';
import { writeFileSync } from 'fs';
import { resolve } from 'path';
import { getInfuraWeb3 } from '../../apis/infura';
import { createErc20Snapshot } from '../../base/erc20';
import { sumBN } from '../../base/utils';
import { formatDateTime } from '../../base/formatting';
import { holdersAsCsv } from '../../base/csv';

function snapshotPath(name: string, time: Date) {
  return `./data/snapshots/${name}-${formatDateTime(time)}.csv`;
}

export async function makeErc20Snapshot(
  name: string,
  address: string,
  outputPath: string = null,
) {
  outputPath = resolve(outputPath ?? snapshotPath(name, new Date()));
  console.log(`Making snapshot of '${name}' and saving it to '${outputPath}'.`);
  const web3 = getInfuraWeb3('mainnet');
  const snapshot = await createErc20Snapshot(address, web3);
  const total = sumBN(snapshot.holders.map(x => x.balance));
  console.log(
    `${name}: ${snapshot.holders.length} token holders totaling: ${fromWei(total)}`,
  );
  const csv = holdersAsCsv(snapshot.holders);
  writeFileSync(outputPath, csv);
}
