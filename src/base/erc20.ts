import BN from 'bn.js';
import Web3 from 'web3';
import { t } from './meta';
import { SortedArray } from './sorting';
import type { IERC20 } from '../contracts/IERC20';
import type { ERC20Pausable } from '../contracts/ERC20Pausable';
import type { NonPayableTransactionObject } from '../contracts/types';
import { assertIsAddress, isAddressZero } from './utils';
import { Participant, Recipient } from './models';
import {
  decodeMethod,
  getContract,
  getWeb3,
  Web3Source,
  getBlockTimestamp,
} from './web3';

export type AbstractErc20 = IERC20 & {
  methods: {
    transferToMany(
      _recipients: string[],
      _values: (number | string)[],
    ): NonPayableTransactionObject<boolean>;
    name(): NonPayableTransactionObject<string>;
    symbol(): NonPayableTransactionObject<string>;
  };
} & ERC20Pausable;

export async function getErc20Balance(
  contract: IERC20,
  address: string,
): Promise<BN> {
  assertIsAddress(address);
  const balance = await contract.methods.balanceOf(address).call();
  return new BN(balance);
}

export async function getAllTransferEvents(
  contract: IERC20,
  address?: string,
  fromBlock = 0,
  toBlock: number | 'latest' = 'latest',
) {
  const events = await contract.getPastEvents('Transfer', {
    address,
    fromBlock,
    toBlock,
  });
  type EventType = Parameters<
    Parameters<typeof contract.events.Transfer>[1]
  >[1];
  return (events as unknown) as EventType[];
}

export type TimestampedTransferEvent = {
  blockNumber: number;
  blockTimestamp: number;
  from: string;
  to: string;
  value: string;
};

export async function getAllTransferInfo(
  web3: Web3,
  contract: IERC20,
  address?: string,
  fromBlock = 0,
  toBlock: number | 'latest' = 'latest',
) {
  console.log('Downloading TX events');
  const events = await getAllTransferEvents(
    contract,
    address,
    fromBlock,
    toBlock,
  );
  console.log(`Got ${events.length} events, fetching respective txs...`);
  const eventsWithTimestamp: TimestampedTransferEvent[] = await Promise.all(
    events.map(async e => {
      const tx = await web3.eth.getTransaction(e.transactionHash);
      const blockNumber = tx.blockNumber;
      let blockTimestamp = await getBlockTimestamp(web3, blockNumber);
      return {
        from: e.returnValues.from,
        to: e.returnValues.to,
        value: e.returnValues.value,
        blockNumber,
        blockTimestamp,
      };
    }),
  );

  const sorted = SortedArray.createFromSortedUnsafe(
    eventsWithTimestamp,
    (a, b) => a.blockTimestamp - b.blockTimestamp,
    tx => tx.blockTimestamp,
  );
  return sorted;
}

export async function getAllTransferTransactions(
  web3: Web3,
  contract: IERC20,
  address?: string,
  fromBlock = 0,
  toBlock: number | 'latest' = 'latest',
) {
  console.log('Downloading TX events');
  const events = await getAllTransferEvents(
    contract,
    address,
    fromBlock,
    toBlock,
  );
  console.log(`Got ${events.length} events, fetching respective txs...`);
  const txs = await Promise.all(
    events.map(async e =>
      t(e.transactionHash, await web3.eth.getTransaction(e.transactionHash)),
    ),
  );
  const sorted = SortedArray.createFromUnsorted(
    txs,
    (a, b) => a[0].localeCompare(b[0]),
    tx => tx[0],
  );
  const unique = sorted.uniq().array;
  console.log(`Before sorting: ${txs.length}, after: ${unique.length}`);
  return unique;
}

export async function getTransferEvents(
  contract: IERC20,
  recipients: string[],
) {
  type EventType = Parameters<
    Parameters<typeof contract.events.Transfer>[1]
  >[1];
  return new Promise((resolve, reject) => {
    const result: EventType[] = [];
    const events = contract.events
      .Transfer({
        fromBlock: 0,
        filter: { from: recipients, to: recipients },
      })
      .on('error', error => reject(error))
      .on('data', event => result.push(event));
  });
  // TODO: finish the implementation - resolve the promise on data completed.
}

export async function createErc20Snapshot(
  erc20Address: string,
  web3OrNetwork: Web3Source,
) {
  const web3 = getWeb3(web3OrNetwork);
  const contract = await getContract<IERC20>(erc20Address, web3, {
    type: 'build-artifact',
    contractName: 'IERC20',
  });
  const events = await getAllTransferEvents(contract);
  const holders = await Promise.all(
    events
      .filter(ev => !isAddressZero(ev.returnValues.to))
      .map<Participant>(ev => ({
        address: ev.returnValues.to.toLowerCase(),
      }))
      .sortByAddress()
      .uniq()
      .array.map(async p => {
        return {
          address: p.address,
          balance: await getErc20Balance(contract, p.address),
        };
      }),
  );
  return {
    address: erc20Address,
    contract,
    holders: holders.filter(x => !x.balance.isZero()),
  };
}

export async function similarTransactions(
  web3: Web3,
  contract: AbstractErc20,
  list: Recipient[],
) {
  const txs = await getAllTransferTransactions(web3, contract);
  console.log('Filtering txs');
  const filtered = txs.filter(tx => {
    const result = decodeMethod(contract, tx[1].input);
    if (result.name !== 'transferToMany') return false;
    const addresses = result.params[0].value as string[];
    const amounts = result.params[1].value as string[];

    if (addresses.length != amounts.length) {
      throw new Error(
        `Addresses = ${addresses.length} <-> amounts = ${amounts.entries} mismatch on tx: ${tx[0]}`,
      );
    }

    if (list.length != addresses.length) {
      return false;
    }

    // const recipients = addresses.map((address, idx) => ({
    //   address,
    //   amount: new BN(amounts[idx])
    // }))

    return true;
  });

  return filtered;
}
