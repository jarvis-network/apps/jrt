pragma solidity ^0.5.0;

import './ERC20.sol';
import './MinterRole.sol';

/**
 * @title ERC20Mintable
 * @dev ERC20 minting logic
 */
contract ERC20Mintable is ERC20, MinterRole {
  uint256 public constant MAX_SUPPLY = 565000000 * 1E18;

  function initializeErc20Mintable(address sender) internal {
    MinterRole.initializeMinterRole(sender);
  }

  /**
   * @dev Function to mint tokens
   * @param to The address that will receive the minted tokens.
   * @param value The amount of tokens to mint.
   * @return A boolean that indicates if the operation was successful.
   */
  function mint(address to, uint256 value) public onlyMinter returns (bool) {
    _mint(to, value);
    require(_totalSupply <= MAX_SUPPLY, 'Overcoming of max supply');
    return true;
  }

  uint256[50] private ______gap;
}
