const web3Utils = require('web3-utils');
const Web3EthAbi = require('web3-eth-abi');
const secp256k1 = require('secp256k1');

const versionSignature = '\x19\x01';

const PERMIT_TYPEHASH = web3Utils.soliditySha3(
  'Permit(address holder,address spender,uint256 nonce,uint256 expiry,bool allowed)',
);

function getDomainSeparator(networkId, contract) {
  const domainSeparatorBodyTypes = [
    'bytes32',
    'bytes32',
    'bytes32',
    'uint256',
    'address',
  ];
  const domainSeparatorBodyParametres = [
    web3Utils.soliditySha3(
      'EIP712Domain(string name,string version,uint256 chainId,address verifyingContract)',
    ),
    web3Utils.soliditySha3(web3Utils.utf8ToHex('Jarvis Reward Token')),
    web3Utils.soliditySha3(web3Utils.utf8ToHex('1')),
    networkId,
    contract,
  ];
  const domainSeparatorBody = Web3EthAbi.encodeParameters(
    domainSeparatorBodyTypes,
    domainSeparatorBodyParametres,
  );
  return web3Utils.soliditySha3Raw(domainSeparatorBody).replace('0x', '');
}

function getPermitMessageDigest(holder, spender, nonce, expiry, allowed) {
  const messageTypes = [
    'bytes32',
    'address',
    'address',
    'uint256',
    'uint256',
    'bool',
  ];
  const messageParametres = [
    PERMIT_TYPEHASH,
    holder,
    spender,
    nonce,
    expiry,
    allowed,
  ];
  const messageBody = Web3EthAbi.encodeParameters(
    messageTypes,
    messageParametres,
  );
  return web3Utils.soliditySha3(messageBody).replace('0x', '');
}

function createPermitMessageBody(
  holder,
  spender,
  nonce,
  expiry,
  allowed,
  networkId,
  contract,
) {
  /* ----------------------------- Create Deposit message ----------------------------- */
  const messagePayload = getPermitMessageDigest(
    holder,
    spender,
    nonce,
    expiry,
    allowed,
  );

  /* ----------------------------------- End ---------------------------------- */

  /* -------------------------- Create Domain Message ------------------------- */

  const domainPayload = getDomainSeparator(networkId, contract);

  /* ----------------------------------- End ---------------------------------- */

  const versionSignatureHex = web3Utils.utf8ToHex(versionSignature);
  const digestBody = versionSignatureHex.concat(domainPayload, messagePayload);
  const body = web3Utils.soliditySha3(digestBody);
  return new Uint8Array(Buffer.from(body.replace('0x', ''), 'hex'));
}

function sign(message, privKey) {
  const privKeyHex = new Uint8Array(Buffer.from(privKey, 'hex'));
  const sigHex = secp256k1.ecdsaSign(message, privKeyHex);
  const sig = Buffer.from(sigHex.signature).toString('hex');
  const hexInit = '0x';
  const rSig = hexInit.concat(sig.substr(0, 64));
  const sSig = hexInit.concat(sig.substr(64, 64));
  const vSig = sigHex.recid + 27;
  return { v: vSig, r: rSig, s: sSig };
}

function generatePermitSignature(
  holder,
  spender,
  nonce,
  expiry,
  allowed,
  networkId,
  contract,
  privKey,
) {
  const message = createPermitMessageBody(
    holder,
    spender,
    nonce,
    expiry,
    allowed,
    networkId,
    contract,
  );

  return sign(message, privKey);
}

function generatePermitTxPayload(
  holder,
  spender,
  nonce,
  expiry,
  allowed,
  signature,
) {
  return (payloadPermit = web3.eth.abi.encodeFunctionCall(
    {
      name: 'permit',
      type: 'function',
      inputs: [
        {
          type: 'address',
          name: 'holder',
        },
        {
          type: 'address',
          name: 'spender',
        },
        {
          type: 'uint256',
          name: 'nonce',
        },
        {
          type: 'uint256',
          name: 'expiry',
        },
        {
          type: 'bool',
          name: 'allowed',
        },
        {
          type: 'uint8',
          name: 'v',
        },
        {
          type: 'bytes32',
          name: 'r',
        },
        {
          type: 'bytes32',
          name: 's',
        },
      ],
    },
    [
      holder,
      spender,
      nonce,
      expiry,
      allowed,
      signature.v,
      signature.r,
      signature.s,
    ],
  ));
}

module.exports = {
  generatePermitSignature,
  generatePermitTxPayload,
};
