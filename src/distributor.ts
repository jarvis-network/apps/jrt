import _ from 'lodash';
import BN from 'bn.js';
import Web3 from 'web3';
import { PromiEvent, TransactionReceipt } from 'web3-core';

import { sumBN, fromBNToNumber } from './base/utils';
import { t } from './base/meta';
import { Recipient } from './base/models';
import { formatNum, padNumberUpToMax } from './base/formatting';
import { SortedArray } from './base/sorting';
import { AbstractErc20 } from './base/erc20';
import { once, TxObjFeeEstimator, logTransactionStatus } from './base/web3';

export class Distributor {
  constructor(
    public readonly web3: Web3,
    entriesSorted: SortedArray<Recipient, string>,
    public readonly acc: string,
    public readonly jrt: AbstractErc20,
    public readonly estimator?: TxObjFeeEstimator,
    public readonly entries: Recipient[] = entriesSorted.array,
  ) {}

  async distributeRetry(start: number = 0, end: number) {
    const max_retries_on_single = 10;
    let retries_left = max_retries_on_single;
    let current_retries = 0;
    let txSuccessful = false;
    while (start !== end) {
      const subset = this.entries.slice(start, end);
      const recipients = subset.map(e => e.address);
      const amounts = subset.map(e => e.amount);
      const retryInfo =
        retries_left < 10
          ? ` (${retries_left} / ${max_retries_on_single} retries left)`
          : '';
      const detail =
        end - start < 10 ? `-> ${JSON.stringify(recipients)}${retryInfo}` : '';
      const indent =
        current_retries > 0 ? _.repeat(' ', current_retries) + '> ' : '';

      try {
        const totalTokens = Number.parseFloat(
          this.web3.utils.fromWei(sumBN(amounts), 'ether').toString(),
        ).toFixed(2);
        console.log(
          `${indent}Trying to call 'transferToMany' with ${amounts.length} / [${start}, ${end}) addresses, ${totalTokens} tokens${detail}...`,
        );

        this.web3.eth.transactionConfirmationBlocks = 2;

        const txObj = this.jrt.methods.transferToMany(
          recipients,
          amounts.map(a => a.toString(10)),
        );

        if (this.estimator) {
          const { allowance, gas, feeEth, feeUsd } = await this.estimator(
            txObj,
          );
          if (!gas) {
            console.log(
              `Estimated gas usage likely exceeds the allowance of ${allowance}`,
            );
          } else {
            console.log(
              `Expected gas usage: ${gas} | estimated fee in USD: ${feeUsd} | estimated fee in ETH: ${feeEth}`,
            );
          }
        }

        const promiEvent = txObj.send({
          from: this.acc,
        });

        const tx = await logTransactionStatus(this.web3, promiEvent);
        txSuccessful = true;

        current_retries = 0;
        retries_left = max_retries_on_single;
        console.log(`### 'transferToMany' succeeded with [${start}, ${end}).`);
        console.log(
          `Tx: '${tx.transactionHash}', Cumulative Gas Used: ${tx.cumulativeGasUsed}\n`,
        );
        return t(end, tx);
      } catch (error) {
        if (error instanceof Error) {
          console.log(`${indent}Error: `, error);
        } else if (typeof error === 'object') {
          console.log(`${indent}Error: '${JSON.stringify(error)}'`);
        } else {
          console.log(`${indent}Error: '${error}'`);
        }
        if (txSuccessful) continue;
        if (end - start > 1) {
          current_retries++;
          end -= ((end - start) / 2) >> 0;
        } else {
          retries_left--;
          if (retries_left == 0) {
            throw error;
          }
        }
      }
    }
  }

  async distribute(start = 0, end = this.entries.length, maxStep = 250) {
    console.log(`Starting distribution...`);
    let txs: TransactionReceipt[] = [];
    while (start < end) {
      const count = end - start;
      const [completedUntil, tx] = await this.distributeRetry(
        start,
        start + Math.min(count, maxStep),
      );
      start = completedUntil;
      txs.push(tx);
    }
    const totalGasSpent = sumBN(
      _.map(txs, tx => new BN(tx.gasUsed)),
    ).toString();
    console.log(`Distribution completed successfully.`);
    console.log(`Total gas spent: ${totalGasSpent}`);
  }

  async verifyBalances(start: number, end: number) {
    if (!this.entries.length) {
      console.log('No recipients to verify');
      return;
    }

    const maxAmount = _.maxBy(this.entries.slice(start, end), e =>
      fromBNToNumber(e.amount),
    ).amount;

    const formatJRT = (bn: BN) =>
      formatNum(fromBNToNumber(maxAmount), bn) + ' JRT';

    console.log(`Verifying recipients' balances...`);
    let errors = 0;
    for (let i = start; i < end; i++) {
      let bal = new BN(
        await this.jrt.methods.balanceOf(this.entries[i].address).call(),
      );
      const progress = `${padNumberUpToMax(i + 1, end, 0)}/${end}`;
      if (!bal.eq(this.entries[i].amount)) {
        errors++;
        const additionalInfo = this.entries[i].email
          ? ` | ${this.entries[i].email}`
          : '';
        console.error(
          `${progress} ERROR on ${this.entries[i].address}${additionalInfo}:`,
        );
        console.error(`Expected: ${this.entries[i].amount}`);
        console.error(`  Actual: ${bal}`);
      } else {
        console.log(
          `${progress} ${this.entries[i].address} -> ${formatJRT(
            this.entries[i].amount,
          )} | ${this.entries[i].email || this.entries[i].reason}`,
        );
      }
    }
    console.log(
      `Verification complete. ${
        errors == 0 ? 'No errors found.' : `${errors} errors found.`
      }`,
    );
  }

  async paused() {
    return await this.jrt.methods.paused().call();
  }

  async pause() {
    if ((await this.paused()) === false) {
      console.log('Pausing JRT...');
      const res = await this.jrt.methods.pause().send({ from: this.acc });
      console.log(`Tx: ${res.transactionHash}`);
    }
    console.log('JRT paused.');
  }

  async unpause() {
    if ((await this.paused()) === true) {
      console.log('Unpausing JRT...');
      const res = await this.jrt.methods.unpause().send({ from: this.acc });
      console.log(`Tx: ${res.transactionHash}`);
    }
    console.log('JRT unpaused.');
  }
}
