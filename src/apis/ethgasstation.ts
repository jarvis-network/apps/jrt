import axios from 'axios';
import { t } from '../base/meta';

export interface GasPrices {
  fast: number;
  fastest: number;
  safeLow: number;
  average: number;
  block_time: number;
  blockNum: number;
  speed: number;
  safeLowWait: number;
  avgWait: number;
  fastWait: number;
  fastestWait: number;
  gasPriceRange: {
    [gasPrice: string]: number;
  };
}

export async function getEthGasPrices(): Promise<GasPrices> {
  const response = await axios.get(
    'https://ethgasstation.info/json/ethgasAPI.json',
  );
  return response.data as GasPrices;
}

export type BestPrice = {
  bestPriceWei: number;
  bestPriceWaitTimeMinutes: number | 'unknown';
};

/**
 * Returns cheapest gas price, for which the transaction confirmation time
 * should be less then maxDesiredWaitTimeMins.
 * @param maxWaitTime - Specifies the max desired wait time in minutes.
 */
export async function getBestEthGasPrice(maxWaitTime = 2): Promise<BestPrice> {
  try {
    const gasPrices = await getEthGasPrices();
    const range = Object.entries(gasPrices.gasPriceRange).map(([k, v]) =>
      t(Number.parseFloat(k), v),
    );
    const best = range.find(([gas, time]) => time < maxWaitTime);
    return {
      bestPriceWei: (best[0] / 10) * 1e9,
      bestPriceWaitTimeMinutes: best[1],
    };
  } catch (_) {
    return {
      bestPriceWei: 10 * 1e9,
      bestPriceWaitTimeMinutes: 'unknown',
    };
  }
}
