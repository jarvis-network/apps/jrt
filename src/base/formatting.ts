import BN from 'bn.js';
import { fromWei } from 'web3-utils';

export function parseDateTime(dateTime: string) {
  return new Date(parseDate(dateTime).getTime() + parseTime(dateTime).getTime());
}

function parseIntAt(input: string, start: number, end: number): number | null {
  return end > input.length ? undefined : Number.parseInt(input.slice(start, end));
}

export function parseDate(date: string) {
  const Y = parseIntAt(date, 0, 4) ?? 1970;
  const M = parseIntAt(date, 5, 7);
  const D = parseIntAt(date, 8, 10);
  if (D) return new Date(Date.UTC(Y, M - 1, D));
  if (M) return new Date(Date.UTC(Y, M - 1));
  if (Y) return new Date(Date.UTC(Y, 0));
  return new Date(Date.UTC(1970, 0));
}

export function parseTime(time: string) {
  const start = 11;
  const h = parseIntAt(time, start, start + 2);
  const m = parseIntAt(time, start + 3, start + 5);
  const s = parseIntAt(time, start + 6, start + 8);
  if (s) return new Date(Date.UTC(1970, 0, 1, h, m, s));
  if (m) return new Date(Date.UTC(1970, 0, 1, h, m));
  if (h) return new Date(Date.UTC(1970, 0, 1, h));
  return new Date(Date.UTC(1970, 0, 1));
}

export function formatDateTime(date: Date) {
  return `${formatDate(date)}_${formatTime(date)}`;
}

export function formatDate(date: Date) {
  const Y = padNumber(date.getUTCFullYear(), 4, '0', 0);
  const M = padNumber(date.getUTCMonth() + 1, 2, '0', 0);
  const D = padNumber(date.getUTCDate(), 2, '0', 0);
  return `${Y}-${M}-${D}`;
}
export function formatTime(date: Date) {
  const h = padNumber(date.getUTCHours(), 2, '0', 0);
  const m = padNumber(date.getUTCMinutes(), 2, '0', 0);
  const s = padNumber(date.getUTCSeconds(), 2, '0', 0);
  return `${h}-${m}-${s}`;
}

export function padNumberUpToMax(
  num: number,
  max: number,
  fractionalDigits = 2,
  pad = ' ',
) {
  const maxLength = max.toFixed(fractionalDigits).length;
  return padNumber(num, maxLength, ' ', fractionalDigits);
}

export function formatNum(max: number, bn: BN, fractionalDigits = 2) {
  return padNumberUpToMax(
    Number.parseFloat(fromWei(bn)),
    max,
    fractionalDigits,
    ' ',
  );
}

export function padNumber(
  num: number,
  maxLength: number,
  padChar = '0',
  fractionalDigits = 2,
) {
  return num.toFixed(fractionalDigits).padStart(maxLength, padChar);
}
