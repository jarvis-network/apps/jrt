import _ from 'lodash';
import BN from 'bn.js';
import { readdirSync } from 'fs';
import { env } from '../../base/config';
import {
  fromBNToDecimalString,
  toBN,
  sumBN,
  loadJSON,
  isAddressZero,
} from '../../base/utils';
import { Holder, Recipient, saveRecipientsFile } from '../../base/models';
import { parseCsvErc20Snapshot } from '../../base/csv';
import { parseDateTime, formatDate, parseDate } from '../../base/formatting';
import {
  addDays,
  findElementWithClosestTimestamp,
} from '../../base/date-time-utils';

interface SaveRewardsCalculationProps {
  beforeSnapshotPath: string;
  afterSnapshotPath: string;
  outputPath: string;
  reward: BN;
  amountInWei?: boolean;
}

export async function saveRewardsFileBaseOnSnapshots({
  beforeSnapshotPath,
  afterSnapshotPath,
  outputPath,
  reward,
  amountInWei = true,
}: SaveRewardsCalculationProps) {
  const before = parseCsvErc20Snapshot(beforeSnapshotPath);
  const after = parseCsvErc20Snapshot(afterSnapshotPath).map(x => x.address);
  console.log(
    '[1/2] Generating distribution plan based on:\n' +
      `  snapshot 1: '${beforeSnapshotPath}'\n  snapshot 2: '${afterSnapshotPath}'`,
  );
  const result = calculateDailyLiquidityReward(before, after, reward);
  await saveRecipientsFile(result.recipients, outputPath, amountInWei);

  console.log(`[2/2] Distribution plan saved to '${outputPath}'`);
  console.log(
    '  Overview:\n' +
      `    * ${
        result.recipients.length
      } recipeints totaling: ${fromBNToDecimalString(
        result.totalWinnersBalance,
      )}.`,
  );
  console.log(
    `    * Tokens to send: ${fromBNToDecimalString(result.actualReward)}.`,
  );
  console.log(`    * Loosers: ${JSON.stringify(result.loosers, null, 2)}`);
  console.log('-------------');
  return result;
}

type CalculationResult = {
  recipients: Recipient[];
  loosers: Holder[];
  totalWinnersBalance: BN;
  actualReward: BN;
};

export function calculateDailyLiquidityReward(
  holdersBefore: Holder[],
  holderAddressesAfter: string[],
  dailyReward: BN,
): CalculationResult {
  const [winners, loosers] = _.partition(
    holdersBefore.filter(x => !isAddressZero(x.address)),
    h => holderAddressesAfter.includes(h.address),
  );
  const totalWinnersBalance = sumBN(winners.map(x => x.balance));
  const recipients: Recipient[] = winners.map(w => ({
    address: w.address,
    amount: w.balance.mul(dailyReward).div(totalWinnersBalance),
  }));
  const actualReward = sumBN(recipients.map(x => x.amount));
  return { recipients, loosers, totalWinnersBalance, actualReward };
}

type Pool = {
  name: string;
  reward: BN;
  beforeSnapshotPath: string;
  afterSnapshotPath: string;
  outputPath: string;
  date: Date;
  distributionPlan?: CalculationResult;
};

function distributionPath(name: string, time: Date) {
  return `./data/token_distributions/liquidity-provider-rewards/${name}-${formatDate(
    time,
  )}.json`;
}

export async function calculateAllRewards(beforeDate: Date) {
  const rewardSizeDir = 'data/reward-size';
  const dates = readdirSync(rewardSizeDir);
  const afterDate = addDays(beforeDate, 1);
  const closestDate = findElementWithClosestTimestamp(
    dates,
    afterDate,
    parseDate,
  );

  if (closestDate[0] < 0 || closestDate[0] >= dates.length)
    throw new Error(
      `No matching reward-size file for date '${formatDate(afterDate)}'`,
    );

  const rewardInfo = loadJSON(`${rewardSizeDir}/${closestDate[1]}`);
  console.log(
    `Using rewards config from '${rewardSizeDir}/${closestDate[1]}': `,
    rewardInfo,
  );
  const pools = Object.entries(env.ethAddresses.liquidityPools)
    .map(([pool]) => {
      if (!(pool in rewardInfo)) {
        console.log(
          `Skipping pool '${pool}' as its reward size was not found in '${rewardSizeDir}/${closestDate[1]}'`,
        );
        return null;
      }
      const snapshotBefore = getClosestSnapshot(pool, beforeDate);
      const snapshotAfter = getClosestSnapshot(pool, afterDate);
      if (!snapshotBefore || !snapshotAfter) {
        if (!snapshotBefore)
          console.log(
            `Can't find snapshot of ${pool} for ${formatDate(beforeDate)}`,
          );
        if (!snapshotAfter)
          console.log(
            `Can't find snapshot of ${pool} for ${formatDate(afterDate)}`,
          );
        return null;
      }
      const result: Pool = {
        name: pool,
        reward: toBN(rewardInfo[pool]),
        date: beforeDate,
        beforeSnapshotPath: snapshotBefore.path,
        afterSnapshotPath: snapshotAfter.path,
        outputPath: distributionPath(pool, beforeDate),
      };
      return result;
    })
    .filter(pool => !!pool);

  for (let pool of pools) {
    console.log(`Processing pool '${pool.name}'`);
    const result = await saveRewardsFileBaseOnSnapshots({
      ...pool,
      amountInWei: beforeDate.getTime() > parseDate('2020-06-12').getTime(),
    });
    pool.distributionPlan = result;
  }

  return pools;
}

export function getAllSnapshots(
  poolName: string,
): { path: string; date: Date }[] {
  const snapshotsDir = 'data/snapshots';
  const extension = '.csv';
  const snapshots = readdirSync(snapshotsDir)
    .filter(name => name.startsWith(`${poolName}-`) && name.endsWith('.csv'))
    .map(name => ({
      path: `${snapshotsDir}/${name}`,
      date: parseDateTime(
        name.slice(poolName.length + 1, name.length - extension.length),
      ),
    }));
  return snapshots;
}

export function getClosestSnapshot(
  poolName: string,
  date: Date,
): { path: string; date: Date } {
  const snapshots = getAllSnapshots(poolName);
  return findElementWithClosestTimestamp(snapshots, date, s => s.date, true)[1];
}
