require('dotenv').config();
const { Contracts, SimpleProject, ZWeb3 } = require('zos-lib');
const truffleAssert = require('truffle-assertions');
const ethers = require('ethers');
ZWeb3.initialize(web3.currentProvider);
const JarvisRewardTokenV0 = Contracts.getFromLocal('JarvisRewardTokenV0');
const JarvisRewardTokenNew = Contracts.getFromLocal('JarvisRewardToken');
const {
  generatePermitSignature,
  generatePermitTxPayload,
} = require('../utils/metasignature.js');

contract('JRT token first upgrade (mintable feature)', async accounts => {
  before(async () => {
    this.owner = accounts[0];
    this.firstAccount = accounts[1];
    this.secondAccount = accounts[2];
    this.thirdAccount = accounts[3];
    this.networkId = await web3.eth.net.getId();
    const project = new SimpleProject('MyProject', { from: this.owner });
    console.log('Creating an upgradeable instance of V0...');
    this.proxy = await project.createProxy(JarvisRewardTokenV0);
    const mnemonic = process.env.DEV_MNEMONIC;
    const path = "m/44'/60'/0'/0/2";
    const wallet = ethers.Wallet.fromMnemonic(mnemonic, path);
    this.secondAccountPrivKey = wallet.privateKey.replace('0x', '');
    this.getRandomInt = function (min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min)) + min;
    };
  });
  it('Upgrading proxy', async () => {
    //initialization of the proxy with the minting of total supply
    const payloadInit = web3.eth.abi.encodeFunctionCall(
      {
        name: 'initialize',
        type: 'function',
        inputs: [],
      },
      [],
    );
    await web3.eth.sendTransaction({
      from: this.firstAccount,
      to: this.proxy.address,
      data: payloadInit,
      gas: 600000,
    });
    const totSupply = await this.proxy.methods
      .totalSupply()
      .call({ from: this.firstAccount });
    assert.equal(
      web3.utils
        .toBN(totSupply)
        .eq(web3.utils.toBN(web3.utils.toWei((420000000).toString()))),
      true,
      'Wrong inizialization',
    );
    let firstAccountBalance = await this.proxy.methods
      .balanceOf(this.firstAccount)
      .call({ from: this.firstAccount });
    assert.equal(
      web3.utils.toBN(totSupply).eq(web3.utils.toBN(firstAccountBalance)),
      true,
      'Wrong inizialization transfer',
    );
    const transferAmount = this.getRandomInt(1, 420000000);
    const transferAmountWei = web3.utils.toWei(transferAmount.toString());
    const payloadTransfer = web3.eth.abi.encodeFunctionCall(
      {
        name: 'transfer',
        type: 'function',
        inputs: [
          {
            type: 'address',
            name: '_to',
          },
          {
            type: 'uint256',
            name: '_amount',
          },
        ],
      },
      [this.secondAccount, transferAmountWei],
    );
    await web3.eth.sendTransaction({
      from: this.firstAccount,
      to: this.proxy.address,
      data: payloadTransfer,
    });
    let secondAccountBalance = await this.proxy.methods
      .balanceOf(this.secondAccount)
      .call({ from: this.firstAccount });
    assert.equal(
      web3.utils
        .toBN(transferAmountWei)
        .eq(web3.utils.toBN(secondAccountBalance)),
      true,
      'Wrong transfer amount',
    );
    console.log('Upgrading to v1...');
    //const instance = await project.upgradeProxy(proxy.address, JarvisRewardTokenNew);
    //upgrading logical part
    Contracts.setArtifactsDefaults({ gas: 8e6, gasPrice: 20e9 });
    const instance = await JarvisRewardTokenNew.new();
    const payloadAdmin = web3.eth.abi.encodeFunctionCall(
      {
        name: 'upgradeTo',
        type: 'function',
        inputs: [
          {
            type: 'address',
            name: 'newImplementation',
          },
        ],
      },
      [instance.address],
    );
    await web3.eth.sendTransaction({
      from: this.owner,
      to: this.proxy.address,
      data: payloadAdmin,
      gas: 300000,
    });
    //initialize new proxy
    const payloadUpgrade = web3.eth.abi.encodeFunctionCall(
      {
        name: 'upgradeToV2',
        type: 'function',
        inputs: [
          {
            type: 'uint256',
            name: 'chainId',
          },
          {
            type: 'address',
            name: 'minter',
          },
        ],
      },
      [this.networkId, this.firstAccount],
    );
    await web3.eth.sendTransaction({
      from: this.firstAccount,
      to: this.proxy.address,
      data: payloadUpgrade,
      gas: 300000,
    });
    const upgradeSecondAccountBalance = await this.proxy.methods
      .balanceOf(this.secondAccount)
      .call({ from: this.firstAccount });
    //Check correctness of slots and no breaking
    assert.equal(
      web3.utils
        .toBN(upgradeSecondAccountBalance)
        .eq(web3.utils.toBN(secondAccountBalance)),
      true,
      'Wrong second account balance',
    );
    const upgradefirstAccountBalance = await this.proxy.methods
      .balanceOf(this.firstAccount)
      .call({ from: this.firstAccount });
    assert.equal(
      web3.utils
        .toBN(upgradeSecondAccountBalance)
        .add(web3.utils.toBN(upgradefirstAccountBalance))
        .eq(web3.utils.toBN(totSupply)),
      true,
      'Wrong total balance',
    );
  });
  it('Mint function', async () => {
    const proxyAddress = this.proxy.address;
    this.proxy = JarvisRewardTokenNew;
    this.proxy.options.address = proxyAddress;
    const isMinter = await this.proxy.methods
      .isMinter(this.firstAccount)
      .call({ from: this.firstAccount });
    assert.equal(isMinter, true, 'First account has not minter grant');
    const firstAccountBalance = await this.proxy.methods
      .balanceOf(this.firstAccount)
      .call({ from: this.firstAccount });
    const maxAmount = Math.floor(
      web3.utils.fromWei((565000000 - 420000000).toString()),
    );
    const mintAmount = web3.utils.toWei(
      this.getRandomInt(1, maxAmount).toString(),
    );
    const payloadMint = web3.eth.abi.encodeFunctionCall(
      {
        name: 'mint',
        type: 'function',
        inputs: [
          {
            type: 'address',
            name: '_to',
          },
          {
            type: 'uint256',
            name: '_amount',
          },
        ],
      },
      [this.thirdAccount, mintAmount],
    );
    await web3.eth.sendTransaction({
      from: this.firstAccount,
      to: this.proxy.address,
      data: payloadMint,
      gas: 400000,
    });
    const thirdAccountBalance = await this.proxy.methods
      .balanceOf(this.thirdAccount)
      .call({ from: this.firstAccount });
    assert.equal(
      web3.utils.toBN(thirdAccountBalance).eq(web3.utils.toBN(mintAmount)),
      true,
      'Wrong mint function',
    );
  });
  it('Revert without minter grant', async () => {
    const isMinter = await this.proxy.methods
      .isMinter(this.secondAccount)
      .call({ from: this.secondAccount });
    assert.equal(isMinter, false, 'Second account has minter grant');
    const secondAccountBalance = await this.proxy.methods
      .balanceOf(this.secondAccount)
      .call({ from: this.secondAccount });
    const maxAmount = Math.floor(web3.utils.fromWei(secondAccountBalance));
    const mintAmount = web3.utils.toWei(
      this.getRandomInt(1, maxAmount).toString(),
    );
    const payloadMint = web3.eth.abi.encodeFunctionCall(
      {
        name: 'mint',
        type: 'function',
        inputs: [
          {
            type: 'address',
            name: '_to',
          },
          {
            type: 'uint256',
            name: '_amount',
          },
        ],
      },
      [this.secondAccount, mintAmount],
    );
    await truffleAssert.reverts(
      web3.eth.sendTransaction({
        from: this.secondAccount,
        to: this.proxy.address,
        data: payloadMint,
        gas: 400000,
      }),
    );
  });
  it('Revert if overcome max supply during minting', async () => {
    const maxAmount = web3.utils.toBN(
      web3.utils.fromWei(
        await this.proxy.methods.MAX_SUPPLY().call({ from: this.firstAccount }),
      ),
    );
    const actualAmount = web3.utils.toBN(
      web3.utils.fromWei(
        await this.proxy.methods
          .totalSupply()
          .call({ from: this.firstAccount }),
      ),
    );
    const mintAmount = web3.utils.toWei(
      maxAmount.sub(actualAmount).add(web3.utils.toBN('1')),
    );
    const payloadMint = web3.eth.abi.encodeFunctionCall(
      {
        name: 'mint',
        type: 'function',
        inputs: [
          {
            type: 'address',
            name: '_to',
          },
          {
            type: 'uint256',
            name: '_amount',
          },
        ],
      },
      [this.secondAccount, mintAmount],
    );
    await truffleAssert.reverts(
      web3.eth.sendTransaction({
        from: this.firstAccount,
        to: this.proxy.address,
        data: payloadMint,
        gas: 400000,
      }),
    );
  });
  it('Add minter', async () => {
    const payloadAddMinter = web3.eth.abi.encodeFunctionCall(
      {
        name: 'addMinter',
        type: 'function',
        inputs: [
          {
            type: 'address',
            name: 'account',
          },
        ],
      },
      [this.secondAccount],
    );
    await web3.eth.sendTransaction({
      from: this.firstAccount,
      to: this.proxy.address,
      data: payloadAddMinter,
    });
    const isMinter = await this.proxy.methods
      .isMinter(this.secondAccount)
      .call({ from: this.secondAccount });
    assert.equal(isMinter, true, 'Second account not added as minter');
    const secondAccountBalance = await this.proxy.methods
      .balanceOf(this.secondAccount)
      .call({ from: this.secondAccount });
    const maxAmount = web3.utils.toBN(
      web3.utils.fromWei(
        await this.proxy.methods.MAX_SUPPLY().call({ from: this.firstAccount }),
      ),
    );
    const actualAmount = web3.utils.toBN(
      web3.utils.fromWei(
        await this.proxy.methods
          .totalSupply()
          .call({ from: this.firstAccount }),
      ),
    );
    const mintAmount = web3.utils.toWei(
      this.getRandomInt(1, maxAmount.sub(actualAmount)).toString(),
    );
    const payloadMint = web3.eth.abi.encodeFunctionCall(
      {
        name: 'mint',
        type: 'function',
        inputs: [
          {
            type: 'address',
            name: '_to',
          },
          {
            type: 'uint256',
            name: '_amount',
          },
        ],
      },
      [this.secondAccount, mintAmount],
    );
    await web3.eth.sendTransaction({
      from: this.secondAccount,
      to: this.proxy.address,
      data: payloadMint,
      gas: 400000,
    });
    const newSecondAccountBalance = await this.proxy.methods
      .balanceOf(this.secondAccount)
      .call({ from: this.secondAccount });
    assert.equal(
      web3.utils
        .toBN(mintAmount)
        .add(web3.utils.toBN(secondAccountBalance))
        .eq(web3.utils.toBN(newSecondAccountBalance)),
      true,
      'Wrong minting balance for second account',
    );
  });
  it('Remove minter', async () => {
    const payloadRemoveMinter = web3.eth.abi.encodeFunctionCall(
      {
        name: 'renounceMinter',
        type: 'function',
        inputs: [],
      },
      [],
    );
    await web3.eth.sendTransaction({
      from: this.secondAccount,
      to: this.proxy.address,
      data: payloadRemoveMinter,
    });
    const isMinter = await this.proxy.methods
      .isMinter(this.secondAccount)
      .call({ from: this.secondAccount });
    assert.equal(isMinter, false, 'Second account not removed as minter');
  });
  it('Permit function', async () => {
    const signature = generatePermitSignature(
      this.secondAccount,
      this.thirdAccount,
      0,
      0,
      true,
      this.networkId,
      this.proxy.address,
      this.secondAccountPrivKey,
    );
    const payloadPermit = generatePermitTxPayload(
      this.secondAccount,
      this.thirdAccount,
      0,
      0,
      true,
      signature,
    );
    await web3.eth.sendTransaction({
      from: this.firstAccount,
      to: this.proxy.address,
      data: payloadPermit,
    });
    const allowance = web3.utils.toBN(
      await this.proxy.methods
        .allowance(this.secondAccount, this.thirdAccount)
        .call({ from: this.firstAccount }),
    );
    assert.equal(
      allowance.eq(
        web3.utils
          .toBN('2')
          .pow(web3.utils.toBN('256'))
          .sub(web3.utils.toBN('1')),
      ),
      true,
      'Wrong allowance after permit',
    );
    const nonce = await this.proxy.methods
      .nonces(this.secondAccount)
      .call({ from: this.firstAccount });
    assert.equal(nonce, 1, 'Wrong nonce after permit');
  });
  it('Revert conditions of permit function', async () => {
    //Wrong holder
    let signature = generatePermitSignature(
      this.secondAccount,
      this.thirdAccount,
      1,
      0,
      true,
      this.networkId,
      this.proxy.address,
      this.secondAccountPrivKey,
    );
    let payloadPermit = generatePermitTxPayload(
      this.firstAccount,
      this.thirdAccount,
      1,
      0,
      true,
      signature,
    );
    await truffleAssert.reverts(
      web3.eth.sendTransaction({
        from: this.firstAccount,
        to: this.proxy.address,
        data: payloadPermit,
      }),
    );
    //Timeout expired
    const lastTimestamp = (await web3.eth.getBlock('latest')).timestamp;
    signature = generatePermitSignature(
      this.secondAccount,
      this.thirdAccount,
      1,
      lastTimestamp - 1,
      true,
      this.networkId,
      this.proxy.address,
      this.secondAccountPrivKey,
    );
    payloadPermit = generatePermitTxPayload(
      this.secondAccount,
      this.thirdAccount,
      1,
      lastTimestamp - 1,
      true,
      signature,
    );
    await truffleAssert.reverts(
      web3.eth.sendTransaction({
        from: this.firstAccount,
        to: this.proxy.address,
        data: payloadPermit,
      }),
    );
    //Wrong nonce
   signature = generatePermitSignature(
      this.secondAccount,
      this.thirdAccount,
      2,
      0,
      true,
      this.networkId,
      this.proxy.address,
      this.secondAccountPrivKey,
    );
    payloadPermit = generatePermitTxPayload(
      this.secondAccount,
      this.thirdAccount,
      2,
      0,
      true,
      signature,
    );
    await truffleAssert.reverts(
      web3.eth.sendTransaction({
        from: this.firstAccount,
        to: this.proxy.address,
        data: payloadPermit,
      }),
    );
  });
});
