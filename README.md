# JRT Smart Contract and Reward Distribution Code

This repository contains the JRT ECR20 smart contract and related support code.
The JRT smart contract was deployed and is currently managed by a hardware
wallet. This requires a slightly more specific development environment, which
is supported by the provided `Dockerfile`.

## Overview

The support code is written in TypeScript has the following structure:

```
src/
├── apis/
│   └── ...
├── base/
│   └── ...
├── contracts/
│   └── ...
├── jrt-specific/
│   ├── ...
│   └── truffle-config.ts
└── scripts/
    ├── actions/
    │   └── ...
    ├── all-events.ts
    ├── calc-rewards.ts
    ├── erc20-snapshot.ts
    ├── etherscan-csv-to-json-recipent-list.ts
    ├── gen-json-boilerplate.ts
    ├── get-gas-price.ts
    ├── group-recipients.ts
    ├── jrt-playground.ts
    ├── runtime_config.ts
    ├── send_all_lp_rewards.ts
    └── send-to-list.ts
```

* `src/scripts/` contains all user-facing entry-points - a collection of scripts
that automate various parts of the of our internal processes. For example:
  * Running `node dist/scripts/group-recipients.js <csv or json file>` will
    parse a list of recipients in either CSV or JSON format and will output them
    in a canoncial format where the amount is specified in Wei form and
    duplicate addresses are merged (and their amounts summed)
  * Running `node dist/scripts/send-to-list.js <json recipients file>` will
    perform token distribution by signing a transactions via hardware wallet
    (which is assumed to be connected).

## Running the code

Linux is required to run the reward distribution code. Other parts of codebase
could be compiled on other platforms with slight modifications, but this is not
verified.

If you have necessary prerequisites installed already, you should be able to
install the npm dependencies and build the code like so:

```
yarn install && yarn build
```

Setting up a user/dev environment is automated and supported via Docker. You can
build and run the Docker image and then the project with following commanges:

```sh
docker build -t jrt .
docker run --privileged --rm -it -v $PWD:/src jrt sh
yarn install && yarn build # Run inside the container ^
```

(The `$PWD:/src` part of the `docker run` command invocation is used to to mount
the `node_modules/` folder from the host file-system into the Docker one, so
that the `yarn install` step could be run only once (and not every time the
container built or started).)

## Examples

**[ This part of the README file is OUTDATED: ]**

### Test deployment

#### Dev wallet

Deploying on Kovan with a dev wallet:

```sh
npx zos session --close && npx zos session -n kovan --expires 600 --timeout 600 -f 0x1eaC6154227C322d79497Be52a255c8597524f98

npx zos create JarvisRewardToken --skip-compile --init 'initialize()'
```

#### Ledger hardware wallet

Deploying on Kovan with Ledger:

```sh
npx zos session --close && npx zos session -n kovan_ledger --expires 600 --timeout 600 -f 0x0D54AAdd7CE2DC10eb9527C6105a3c3f1b463d1b

npx zos create JarvisRewardToken --skip-compile --init 'initialize()'
```

### Pausing and unpausing the smart contract

* `npm run cli-<network name> pause -c <contract address> -w <owner wallet address>`
* `npm run cli-<network name> unpause -c <contract address> -w <owner wallet address>`

where `<network name>` must be one of: `mainnet`, `kovan-ledger` or `kovan`.

### Token distribution and distribution verification

Distribution and verification is done by supplying a file containing a table of addresses, amounts and descriptions in JSON format.

#### Recipients file format

```json
[
  { "email": "email or a note about recipeint", "amount": "10,100,000.123", "address": "0x..." },
]
```

#### Verification

Verify the balance of each recipeint in a list:

CLI:
`npm run cli-<network name>` - npm script name
  `verify` - script command
  `-c <contract address>` - address of the JRT contract to interact with
  `-w <owner wallet address>` - token sender wallet address
  `<recipients file>` - path to JSON file

Example:

```
npm run cli-mainnet -- verify -c 0x8A9C67fee641579dEbA04928c4BC45F66e26343A ./data/token_distributions/investors_wave13.json
```

#### Distribution

CLI:
`npm run cli-<network name>` - npm script name
  `send` - script command
  `-c <contract address>` - address of the JRT contract to interact with
  `-w <owner wallet address>` - token sender wallet address
  `<recipients file>` - path to JSON file


Example:

```
npm run cli-mainnet -- send -w 0x0D54AAdd7CE2DC10eb9527C6105a3c3f1b463d1b -c 0x8A9C67fee641579dEbA04928c4BC45F66e26343A ./data/token_distributions/investors_wave13.json
```

## Convert CSV to JSON

Etherscan supports exporting the list of ERC20 token holders in CSV format. The resulting CSV file can be converted into JSON like this:

```sh
npm run csv-to-json -- 20200109.csv ./data/token_distributions/uniswap-liquidity-daily-reward-for-2020-01-09.json
```
