import program from 'commander';
import {
  groupByAddressAndSortByAmount,
  recipientFromJson,
  saveRecipientsFile,
  readRecipientsFile,
} from '../base/models';
import { assertIsString } from '../base/utils';
import { parseCsvRecipientList } from '../base/csv';

program
  .requiredOption(
    '-i, --input <path>',
    'Path to a JSON file containing array of recipeints',
  )
  .option('-o, --output <path>', 'Output path')
  .parse(process.argv);

async function main() {
  const list = assertIsString(program.input).endsWith('.csv')
    ? parseCsvRecipientList(program.input).map(recipientFromJson)
    : await readRecipientsFile(program.input);
  const grouped = groupByAddressAndSortByAmount(list).array;
  await saveRecipientsFile(grouped, assertIsString(program.output));
}

(async () => await main())();
