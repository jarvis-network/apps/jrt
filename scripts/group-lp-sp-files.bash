#!/usr/bin/env bash

set euo -pipefail

GIT_START_COMMIT=${GIT_START_COMMIT:-HEAD^}
GIT_END_COMMIT=${GIT_END_COMMIT:-HEAD}
OUTPUT_FOLDER=${OUTPUT_FOLDER:-./all-lp-sp-$(date +'%Y-%m-%d')}
ZIP_FILES=${ZIP_FILES:-0}

mkdir "$OUTPUT_FOLDER"

pushd data
for f in $(git diff-tree --no-commit-id --name-only -r "$GIT_START_COMMIT" "$GIT_END_COMMIT" | sort | uniq); do
    cp -vp --parents "$f" "../$OUTPUT_FOLDER"
done
popd

if [[ $ZIP_FILES -eq 1 ]] || [[ "$ZIP_FILES" == 'true'  ]]; then
    zip -r "${OUTPUT_FOLDER}.zip" "$OUTPUT_FOLDER"
fi
