require('./runtime_config');
import * as fs from 'fs';
import program from 'commander';
import { assertIsString } from '../base/utils';
import {
  calculateTotalReward,
  calculateUserReward,
} from './actions/weight-reward-calculation';
import { calculate } from '../calculations/erc20-time-weighted';
import {
  Snapshot,
  readSnapshotFile,
  writeSnapshotFile,
} from '../calculations/snapshot';
import { parseDateTime, formatDate } from '../base/formatting';
import { env } from '../base/config';
import { distributeTokens } from './actions/distribute';
import { getJrtAndWeb3 } from '../jrt-specific/get-jrt';
import type { ERC20 } from '../contracts/ERC20';
import { getContract } from '../base/web3';
import BN from 'bn.js';
import { recipientListToJson } from '../base/models';

program
  .requiredOption('--snapshotDate <date>', 'Date of previous snapshot')
  .option(
    '--startingTime <number>',
    'Inital time in Unix timestamp from which calcuating rewards',
  )
  .option('--duration <number>', 'Duration of the the snapshot to create')
  .option(
    '--exec',
    'Execute distribution immediately after calculations',
    false,
  )
  .option('--verify', 'Perform verification', false);

program.parse(process.argv);

interface Pool {
  name: string;
  recipients: {
    address: string;
    amount: BN;
  }[];
}

async function runSequentially(promises: Promise<void>[]) {
  for (let p of promises) {
    await p;
  }
}

async function waitSeconds(seconds: number) {
  await new Promise<void>(resolve => {
    setTimeout(() => resolve(), seconds * 1000);
  });
}

(async () => {
  console.log('Starting program.');
  const { jrt, web3 } = await getJrtAndWeb3();
  const snpashotsNames = fs.readdirSync('./data/snapshots');
  let snapshots: Snapshot[] = [];
  let pools: Pool[] = [];
  await runSequentially(
    Object.entries(env.ethAddresses.liquidityPools).map(
      async ([pool, address]) => {
        await waitSeconds(0.1);
        const snapshotPrefix = `${pool}-${program.snapshotDate}`;
        console.log(`Looking for files starting with: ${snapshotPrefix}`);
        const snapshotName = snpashotsNames.filter(function (snapshot: string) {
          return (
            snapshot.startsWith(snapshotPrefix) && snapshot.endsWith('.json')
          );
        })[0];
        const filename = `./data/snapshots/${snapshotName}`;
        console.log(`Reading file name: ${filename}`);
        const prevSnpashot = await readSnapshotFile(filename);
        const startingtime =
          prevSnpashot.endingTime || parseInt(program.startingTime);
        const duration =
          parseInt(env.lpSlotDuration) || parseInt(program.duration);
        console.log(`Starting calculation of ${pool} weights`);
        const erc20 = await getContract<ERC20>(address, web3, {
          type: 'build-artifact',
          contractName: 'ERC20',
        });
        const endTime = new Date((startingtime + duration) * 1000);
        const snapshot = await calculate(
          web3,
          erc20,
          prevSnpashot,
          startingtime,
          duration,
        );
        console.log(`Creating new weight snapshot for pool ${pool}`);
        const currentDate = formatDate(endTime);
        console.log(currentDate);
        await writeSnapshotFile(
          `./data/snapshots/${pool}-${currentDate}.json`,
          snapshot,
        );
        if (Object.keys(snapshot.participants).length === 0) {
          console.log(`No users in pool ${pool}`);
          return;
        }
        console.log(`Calculation of rewards for ${pool}`);

        const reward = calculateTotalReward(
          parseDateTime(assertIsString(program.snapshotDate)),
          duration,
          pool,
        );
        if (reward === null) {
          return;
        } else {
          const recipients = calculateUserReward(snapshot.participants, reward);
          const poolData = { name: pool, recipients: recipients };
          pools.push(poolData);
          console.log(`Creating new reward snapshot for pool ${pool}`);
          fs.writeFileSync(
            `./data/token_distributions/liquidity-provider-rewards/${pool}-${currentDate}.json`,
            JSON.stringify(recipientListToJson(recipients, true), null, 2),
          );
        }
      },
    ),
  );
  console.log('All calculations are terminated');

  if (!program.exec && !program.verify) return;

  console.log(
    `Starting token ${program.exec ? 'distribution' : 'verification'}.`,
  );

  for (let pool of pools) {
    console.log(`Sending tokens for pool '${pool.name}'`);
    await distributeTokens({
      web3,
      erc20: jrt,
      dryRun: !program.exec,
      verify: program.verify,
      listParams: {
        recipients: pool.recipients,
      },
    });
  }
  console.log('Program execution complete.');
})();
